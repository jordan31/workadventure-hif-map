/// <reference path="../node_modules/@workadventure/iframe-api-typings/iframe_api.d.ts" />
import { bootstrapExtra } from "@workadventure/scripting-api-extra";

console.log('Script started successfully');

let currentPopup: any = undefined;

// Waiting for the API to be ready
WA.onInit().then(() => {
    console.log('Scripting API ready');
    console.log('Player tags: ',WA.player.tags)
    
    // Clock Popup Indium
    WA.room.onEnterLayer('clockZoneIndium').subscribe(() => {
        const today = new Date();
        const time = today.getHours() + ":" + today.getMinutes();
        currentPopup = WA.ui.openPopup("clockPopupIndium","It's " + time,[]);
    })

    WA.room.onLeaveLayer('clockZoneIndium').subscribe(closePopUp)

     // Clock Popup Germanium
    WA.room.onEnterLayer('clockZoneGermanium').subscribe(() => {
        const today = new Date();
        const time = today.getHours() + ":" + today.getMinutes();
        currentPopup = WA.ui.openPopup("clockPopupGermanium","It's " + time,[]);
    })

    WA.room.onLeaveLayer('clockZoneGermanium').subscribe(closePopUp)

    // Clock Popup Argentum
    WA.room.onEnterLayer('clockZoneArgentum').subscribe(() => {
        const today = new Date();
        const time = today.getHours() + ":" + today.getMinutes();
        currentPopup = WA.ui.openPopup("clockPopupArgentum","It's " + time,[]);
    })

    WA.room.onLeaveLayer('clockZoneArgentum').subscribe(closePopUp)

    // Rasenlatscher Popup
    WA.room.onEnterLayer('lawn1Zone').subscribe(() => {
        currentPopup = WA.ui.openPopup("lawn1Popup","Oh no the beautiful flowers. Please be a little more careful.",[]);
    })

    WA.room.onLeaveLayer('lawn1Zone').subscribe(closePopUp)
    // ENDE

    // Rasenlatscher Popup rechts
    WA.room.onEnterLayer('lawn2Zone').subscribe(() => {
        currentPopup = WA.ui.openPopup("lawn2Popup","Oh no the beautiful flowers. Please be a little more careful.",[]);
    })

    WA.room.onLeaveLayer('lawn2Zone').subscribe(closePopUp)
    // ENDE


    // Treppe
    WA.room.onEnterLayer('stairsFloor1Zone').subscribe(() => {
        const today = new Date();
        const time = today.getHours() + ":" + today.getMinutes();
        currentPopup = WA.ui.openPopup("stairsPopup","Here you go up to the rooms Indium, Germanium, Argentum, Europium, Cuprum and the computer lab.",[]);
    })

    WA.room.onLeaveLayer('stairsFloor1Zone').subscribe(closePopUp)
     // ENDE

    // The line below bootstraps the Scripting API Extra library that adds a number of advanced properties/features to WorkAdventure
    bootstrapExtra().then(() => {
        console.log('Scripting API Extra ready');
    }).catch(e => console.error(e));
    
}).catch(e => console.error(e));

function closePopUp(){
    if (currentPopup !== undefined) {
        currentPopup.close();
        currentPopup = undefined;
    }
}

